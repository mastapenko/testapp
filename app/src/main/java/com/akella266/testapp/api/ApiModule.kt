package com.akella266.testapp.api

import com.akella266.testapp.data.model.FieldType
import com.akella266.testapp.data.model.FieldTypeDeserializer
import com.akella266.testapp.data.model.GsonMapDeserializer
import com.akella266.testapp.utils.Constants
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
class ApiModule {

    @Singleton
    @Provides
    fun provideRestApi(): RestApi {
        val rxAdapter = RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io())

        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        val okHttp = OkHttpClient.Builder().addInterceptor(interceptor).build()

        val gson = GsonBuilder().registerTypeAdapter(HashMap::class.java, GsonMapDeserializer())
            .registerTypeAdapter(FieldType::class.java, FieldTypeDeserializer()).create()

        return Retrofit.Builder()
            .client(okHttp)
            .baseUrl(Constants.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(rxAdapter)
            .build()
            .create(RestApi::class.java)
    }
}