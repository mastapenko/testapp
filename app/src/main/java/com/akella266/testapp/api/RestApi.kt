package com.akella266.testapp.api

import com.akella266.testapp.data.model.Meta
import com.akella266.testapp.data.model.Request
import com.akella266.testapp.data.model.Result
import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.POST

interface RestApi {

    @POST("meta")
    fun getMeta(): Single<Meta>

    @POST("data")
    fun postData(@Body data: Request): Single<Result>
}