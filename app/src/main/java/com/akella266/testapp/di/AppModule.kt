package com.akella266.testapp.di

import android.content.Context
import com.akella266.testapp.App
import dagger.Module
import dagger.Provides

@Module
class AppModule {

    @Provides
    fun provideContext(application: App): Context = application.applicationContext

}