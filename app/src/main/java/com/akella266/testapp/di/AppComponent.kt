package com.akella266.testapp.di

import com.akella266.testapp.App
import com.akella266.testapp.api.ApiModule
import com.akella266.testapp.data.DataModule
import com.akella266.testapp.viewmodels.ViewModelModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [
    AndroidSupportInjectionModule::class,
    ApiModule::class,
    DataModule::class,
    AppModule::class,
    ViewModelModule::class,
    ActivityBindingModule::class])
interface AppComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: App): Builder
        fun build(): AppComponent
    }
    fun inject(app: App)
}