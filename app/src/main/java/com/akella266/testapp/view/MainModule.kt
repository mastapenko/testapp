package com.akella266.testapp.view

import com.akella266.testapp.di.FragmentScoped
import com.akella266.testapp.viewmodels.MainViewModel
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class MainModule {

    @FragmentScoped
    @ContributesAndroidInjector
    abstract fun mainFragment(): MainFragment
}