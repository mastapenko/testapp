package com.akella266.testapp.view.dialog

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import butterknife.ButterKnife
import butterknife.OnClick
import butterknife.Unbinder
import com.akella266.testapp.R
import kotlinx.android.synthetic.main.dialog_result.*

class InfoDialog: DialogFragment() {

    companion object {
        private val ARGS_TITLE = "args_title"
        private val ARGS_MESSAGE = "args_message"

        fun newInstance(title: String?, message: String?): InfoDialog {
            val dialog = InfoDialog()
            dialog.arguments = Bundle().apply {
                putString(ARGS_TITLE, title)
                putString(ARGS_MESSAGE, message)
            }
            return dialog
        }
    }

    private var unbinder: Unbinder? = null

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        dialog.window?.requestFeature(Window.FEATURE_NO_TITLE)
        if (dialog.window != null) dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        return dialog
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.dialog_result, container, false)
        unbinder = ButterKnife.bind(this, view)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        arguments?.let { args ->
            args.getString(ARGS_TITLE)?.let { result_title.text = it }
            args.getString(ARGS_MESSAGE)?.let { result_message.text = it }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        if (unbinder != null) unbinder?.unbind()
    }

    @OnClick(R.id.result_ok_button)
    fun onOkClicked() {
        dismiss()
    }
}