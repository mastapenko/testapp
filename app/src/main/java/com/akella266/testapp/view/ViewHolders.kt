package com.akella266.testapp.view

import android.text.Editable
import android.text.InputType
import android.text.TextWatcher
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import com.akella266.testapp.R
import com.akella266.testapp.data.model.Field
import kotlinx.android.synthetic.main.item_list_field.view.*
import kotlinx.android.synthetic.main.item_numeric_field.view.*
import kotlinx.android.synthetic.main.item_text_field.view.*

abstract class BaseFieldViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {

    abstract fun bind(field: Field)
}

class ListFieldViewHolder(itemView: View): BaseFieldViewHolder(itemView) {

    override fun bind(field: Field) {
        with(itemView) {
            item_list_name_field.text = field.title
            val adapter = ArrayAdapter<String>(itemView.context, android.R.layout.simple_spinner_item, field.values.values.toList())
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            item_list_spinner.adapter = adapter
            item_list_spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                    field.result = field.values.entries.find { it.value == adapter.getItem(position) }?.key
                }
                override fun onNothingSelected(parent: AdapterView<*>?) {}
            }
        }
    }
}

class TextFieldViewHolder(itemView: View): BaseFieldViewHolder(itemView) {

    override fun bind(field: Field) {
        with(itemView) {
            item_text_name.text = field.title
            item_text_edit_text.setText("")
            item_text_edit_text.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
                override fun afterTextChanged(s: Editable?) {
                    field.result = s?.toString()!!
                }
            })
        }
    }

}

class NumericFieldViewHolder(itemView: View): BaseFieldViewHolder(itemView) {

    override fun bind(field: Field) {
        with(itemView) {
            item_numeric_name.text = field.title
            item_numeric_edit_text.setText("")
            item_numeric_edit_text.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
                override fun afterTextChanged(s: Editable?) {
                    field.result = s?.toString()!!
                }
            })
        }
    }

}