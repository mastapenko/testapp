package com.akella266.testapp.view

import androidx.lifecycle.Observer
import android.os.Bundle
import androidx.fragment.app.Fragment
import butterknife.ButterKnife
import butterknife.Unbinder
import com.akella266.testapp.R
import com.akella266.testapp.data.dto.MetaDto
import com.akella266.testapp.data.model.Status
import com.akella266.testapp.viewmodels.MainViewModel
import javax.inject.Inject
import android.content.Context.CONNECTIVITY_SERVICE
import android.net.ConnectivityManager
import android.os.PersistableBundle
import com.google.android.material.snackbar.Snackbar
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.*
import butterknife.OnClick
import com.akella266.testapp.data.dto.ResultDto
import com.akella266.testapp.data.model.Meta
import com.akella266.testapp.data.model.Result
import com.akella266.testapp.utils.snack
import com.akella266.testapp.view.dialog.InfoDialog
import com.akella266.testapp.view.dialog.InfoDialogBuilder
import com.akella266.testapp.view.dialog.LoadingDialog
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_main.*


class MainFragment @Inject constructor(): Fragment() {

    private lateinit var unbinder: Unbinder

    @Inject
    lateinit var viewModel: MainViewModel
    private lateinit var adapter: FormAdapter
    private var loadingDialog: LoadingDialog? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_main, container, false)
        unbinder = ButterKnife.bind(this, view)
        setHasOptionsMenu(true)
        retainInstance = true
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        adapter = FormAdapter(viewModel)
        main_recycler_view.adapter = adapter
        main_recycler_view.layoutManager = LinearLayoutManager(context!!)

        viewModel.result.observe(this, Observer { processResult(it!!) })
        viewModel.response.observe(this, Observer { processResponse(it!!) })
        if (!viewModel.isDataStartedLoading) {
            loadData()
            viewModel.isDataStartedLoading = true
        }
    }

    override fun onDestroyView() {
        unbinder.unbind()
        super.onDestroyView()
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater?.inflate(R.menu.main_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when(item?.itemId) {
            R.id.menu_update -> loadData()
        }
        return super.onOptionsItemSelected(item)
    }

    @OnClick(R.id.main_send_button)
    fun onSendClicked() {
        if (checkInternetConnection()) {
            viewModel.sendData(adapter.fields!!)
        } else showMessage(getString(R.string.error_no_connection))
    }

    private fun loadData() {
        if (checkInternetConnection()) {
            viewModel.loadMeta()
        } else showMessage(getString(R.string.error_no_connection))
    }

    private fun processResponse(response: MetaDto) {
        when(response.status) {
            Status.LOADING -> showLoading()
            Status.SUCCESS -> {
                populateData(response.data!!)
                hideLoading()
            }
            Status.ERROR -> {
                showMessage(response.error?.message)
                hideLoading()
            }
        }
    }

    private fun populateData(data: Meta) {
        with(data) {
            setTitle(title)
            setImage(image)
            adapter.fields = fields
        }
    }

    private fun setTitle(title: String) {
        activity?.toolbar?.title = title
    }

    private fun setImage(url: String) {
        Glide.with(this).asBitmap().load(url).into(main_image_view)
    }

    private fun checkInternetConnection(): Boolean {
        val cm = context?.getSystemService(CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork = cm.activeNetworkInfo
        return activeNetwork?.isConnected ?: false
    }

    private fun processResult(resultDto: ResultDto) {
        when(resultDto.status) {
            Status.LOADING -> showLoading()
            Status.SUCCESS -> {
                hideLoading()
                InfoDialogBuilder(fragmentManager!!)
                    .setTitle(getString(R.string.dialog_title_result))
                    .setMessage(resultDto.result?.result ?: "")
                    .show()
            }
            Status.ERROR -> {
                showMessage(resultDto.error?.message)
                hideLoading()
            }
        }
    }

    private fun showMessage(message: String?) {
        view?.snack(message!!, Snackbar.LENGTH_LONG) {}
    }

    private fun showLoading() {
        if (loadingDialog == null) {
            loadingDialog = LoadingDialog.show(fragmentManager!!, object : LoadingDialog.CancelListener {
                override fun onCancelClicked() {
                    viewModel.cancelRequest()
                    loadingDialog?.dismiss()
                }
            })
        } else loadingDialog?.dialog?.show()
    }

    private fun hideLoading() {
        if (loadingDialog != null) {
            loadingDialog?.dismiss()
            loadingDialog = null
        }
    }
}