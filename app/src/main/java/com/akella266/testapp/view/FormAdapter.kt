package com.akella266.testapp.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.akella266.testapp.R
import com.akella266.testapp.data.model.Field
import com.akella266.testapp.data.model.FieldType
import com.akella266.testapp.viewmodels.MainViewModel

class FormAdapter(private val viewModel: MainViewModel): RecyclerView.Adapter<BaseFieldViewHolder>() {

    var fields: List<Field>? = null
            set(value) {
                field = value
                notifyDataSetChanged()
            }

    override fun getItemViewType(position: Int): Int = fields?.get(position)?.type?.ordinal!!

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): BaseFieldViewHolder {
        return when(FieldType.values()[p1]) {
            FieldType.TEXT -> {
                val view = LayoutInflater.from(p0.context).inflate(R.layout.item_text_field, p0, false)
                TextFieldViewHolder(view)
            }
            FieldType.NUMERIC -> {
                val view = LayoutInflater.from(p0.context).inflate(R.layout.item_numeric_field, p0, false)
                NumericFieldViewHolder(view)
            }
            FieldType.LIST -> {
                val view = LayoutInflater.from(p0.context).inflate(R.layout.item_list_field, p0, false)
                ListFieldViewHolder(view)
            }
        }
    }

    override fun getItemCount(): Int = fields?.size ?: 0

    override fun onBindViewHolder(p0: BaseFieldViewHolder, p1: Int) {
        p0.bind(fields!![p1])
    }
}