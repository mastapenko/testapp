package com.akella266.testapp.view.dialog

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import butterknife.ButterKnife
import butterknife.OnClick
import butterknife.Unbinder
import com.akella266.testapp.R

class LoadingDialog: DialogFragment() {

    companion object {
        fun newInstance() = LoadingDialog()

        fun show(fragmentManager: FragmentManager, cancelListener: CancelListener): LoadingDialog {
            val dialog = newInstance()
            dialog.cancelListener = cancelListener
            dialog.show(fragmentManager, LoadingDialog::class.simpleName)
            return dialog
        }
    }

    private var unbinder: Unbinder? = null
    private var cancelListener: CancelListener? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.dialog_loading, container, false)
        unbinder = ButterKnife.bind(this, view)
        return view
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        dialog.window?.requestFeature(Window.FEATURE_NO_TITLE)
        if (dialog.window != null) dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        return dialog
    }

    override fun onDestroyView() {
        super.onDestroyView()
        if (unbinder != null) unbinder?.unbind()
    }

    @OnClick(R.id.load_cancel_button)
    fun cancelClick() {
        if (cancelListener != null)
            cancelListener?.onCancelClicked()
    }

    interface CancelListener {
        fun onCancelClicked()
    }
}