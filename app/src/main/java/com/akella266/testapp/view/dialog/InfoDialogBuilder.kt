package com.akella266.testapp.view.dialog

import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager

class InfoDialogBuilder(private val fragmentManager: FragmentManager) {

    private var title: String? = null
    private var message: String? = null

    fun setTitle(title: String) = apply { this.title = title }
    fun setMessage(message: String) = apply { this.message = message }

    fun build(): DialogFragment = InfoDialog.newInstance(title, message)

    fun show(): DialogFragment {
        val dialog = build()
        dialog.show(fragmentManager, dialog.javaClass.simpleName)

        return dialog
    }
}