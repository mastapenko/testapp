package com.akella266.testapp.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.akella266.testapp.data.Repository
import com.akella266.testapp.data.model.Field
import com.akella266.testapp.data.model.Request
import com.akella266.testapp.data.dto.MetaDto
import com.akella266.testapp.data.dto.ResultDto
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

import javax.inject.Inject

class MainViewModel @Inject constructor(val repository: Repository) : ViewModel(){

    private val disposable = CompositeDisposable()
    var isDataStartedLoading = false

    val response = MutableLiveData<MetaDto>()
    val result = MutableLiveData<ResultDto>()

    override fun onCleared() {
        super.onCleared()
        disposable.clear()
    }

    fun loadMeta() {
        disposable.add(repository.getMeta()
            .observeOn(Schedulers.io())
            .subscribeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { response.value = MetaDto.loading()}
            .subscribe({ meta -> response.postValue(MetaDto.success(meta)) },
                { error -> response.postValue(MetaDto.error(error)) }))
    }

    fun sendData(fields: List<Field>) {
        val request = convertToRequest(fields)
        disposable.add(repository.postData(request)
            .observeOn(Schedulers.io())
            .subscribeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { result.value = ResultDto.loading()}
            .subscribe({ rslt -> result.postValue(ResultDto.success(rslt)) },
                { error -> result.postValue(ResultDto.error(error)) }))
    }

    fun cancelRequest() {
        disposable.clear()
    }

    private fun convertToRequest(fields: List<Field>): Request {
        val hashMap = HashMap<String, String>()
        fields.forEach { field -> hashMap[field.name] = field.result ?: "" }
        return Request(hashMap)
    }
}