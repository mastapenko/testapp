package com.akella266.testapp.viewmodels

import androidx.lifecycle.ViewModel
import com.akella266.testapp.di.FragmentScoped
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import javax.inject.Singleton

@Module
abstract class ViewModelModule {

    @FragmentScoped
    @Binds
    abstract fun provideMainViewModel(mainViewModel: MainViewModel): ViewModel
}