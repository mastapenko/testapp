package com.akella266.testapp.utils

import com.google.android.material.snackbar.Snackbar
import android.view.View

inline fun View.snack(message: String, length: Int = Snackbar.LENGTH_LONG, f: Snackbar.() -> Unit) {
    val snack = Snackbar.make(this, message, length)
    snack.f()
    snack.show()
}