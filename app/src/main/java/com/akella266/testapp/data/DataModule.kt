package com.akella266.testapp.data

import com.akella266.testapp.api.RestApi
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DataModule {

    @Singleton
    @Provides
    fun provideRepository(api: RestApi) = Repository(api)
}