package com.akella266.testapp.data

import com.akella266.testapp.data.model.Meta
import com.akella266.testapp.data.model.Request
import com.akella266.testapp.data.model.Result
import io.reactivex.Single
import org.json.JSONObject

interface DataSource {

    fun getMeta(): Single<Meta>
    fun postData(data: Request): Single<Result>
}