package com.akella266.testapp.data.model

enum class Status {
    LOADING,
    ERROR,
    SUCCESS
}