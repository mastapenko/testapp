package com.akella266.testapp.data.dto

import com.akella266.testapp.data.model.Result
import com.akella266.testapp.data.model.Status
import io.reactivex.annotations.Nullable

class ResultDto(val status: Status,
                @Nullable val result: Result?,
                @Nullable val error: Throwable?) {

    companion object {
        fun loading() =
            ResultDto(Status.LOADING, null, null)
        fun success(@Nullable rslt: Result?) =
            ResultDto(Status.SUCCESS, rslt, null)
        fun error(@Nullable error: Throwable?) =
            ResultDto(Status.ERROR, null, error)
    }
}