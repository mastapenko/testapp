package com.akella266.testapp.data.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Meta(var title: String,
                var image: String,
                var fields: List<Field>)

data class Field(var title: String,
                 var name: String,
                 var type: FieldType,
                 var values: HashMap<String, String>,
                 @Expose var result: String?)