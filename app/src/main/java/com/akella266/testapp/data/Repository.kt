package com.akella266.testapp.data

import com.akella266.testapp.api.RestApi
import com.akella266.testapp.data.model.Meta
import com.akella266.testapp.data.model.Request
import com.akella266.testapp.data.model.Result
import io.reactivex.Single
import org.json.JSONObject
import javax.inject.Inject

class Repository @Inject constructor(val api: RestApi): DataSource {

    override fun getMeta(): Single<Meta> =
        api.getMeta()

    override fun postData(data: Request): Single<Result> = api.postData(data)
}