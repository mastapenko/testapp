package com.akella266.testapp.data.model

import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import java.lang.reflect.Type

class FieldTypeDeserializer: JsonDeserializer<FieldType> {

    override fun deserialize(json: JsonElement?, typeOfT: Type?, context: JsonDeserializationContext?): FieldType {
        val type = json?.asString
        return when(type) {
            "TEXT" -> FieldType.TEXT
            "NUMERIC" -> FieldType.NUMERIC
            "LIST" -> FieldType.LIST
            else -> FieldType.TEXT
        }
    }
}