package com.akella266.testapp.data.model

import com.google.gson.*
import java.lang.reflect.Type

class GsonMapDeserializer: JsonDeserializer<HashMap<String, String>>, JsonSerializer<HashMap<String, String>> {

    override fun serialize(src: HashMap<String, String>?, typeOfSrc: Type?, context: JsonSerializationContext?): JsonElement {
        val jsonObject = JsonObject()
        src?.keys?.forEach { key -> jsonObject.addProperty(key, src[key]) }
        return jsonObject
    }

    override fun deserialize(json: JsonElement?, typeOfT: Type?, context: JsonDeserializationContext?): HashMap<String, String> {
        val jsonObject = json?.asJsonObject!!
        val hashMap = HashMap<String, String>()
        jsonObject.keySet().forEach { key -> hashMap[key] = jsonObject[key].asString }
        return hashMap
    }
}