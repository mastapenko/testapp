package com.akella266.testapp.data.model

import com.google.gson.annotations.SerializedName

enum class FieldType {
    @SerializedName("text") TEXT,
    @SerializedName("numeric") NUMERIC,
    @SerializedName("list") LIST
}