package com.akella266.testapp.data.dto

import com.akella266.testapp.data.model.Meta
import com.akella266.testapp.data.model.Status
import io.reactivex.annotations.Nullable

class MetaDto(val status: Status,
              @Nullable val data: Meta?,
              @Nullable val error: Throwable?) {

    companion object {
        fun loading() =
            MetaDto(Status.LOADING, null, null)
        fun success(@Nullable data: Meta?) =
            MetaDto(Status.SUCCESS, data, null)
        fun error(@Nullable error: Throwable?) =
            MetaDto(Status.ERROR, null, error)
    }
}